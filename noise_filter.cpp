#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdlib.h>
#include <time.h>
#include <iostream>

using namespace std;
using namespace cv;

//add salt & pepper noise
bool AddSaltPepperNoise(const Mat src, Mat *dst, double percentage) {
    if (src.empty()) {
        fprintf(stderr, "empty src");
        return false;
    } else if (dst->empty()) {
        fprintf(stderr, "empty dst");
        return false;
    } else if (src.channels() != 1 && src.channels() != 3) {
        fprintf(stderr, "only 1-channel or 3-channel image supported");
        return false;
    }

    srand(time(NULL));

    //total is the number of noise pixels
    int total = percentage * src.rows * src.cols;

    for (int i = 0; i < total; i++) {
        //pick a pixel to be black or white
        int r = rand() % src.rows;
        int c = rand() % src.cols;
        if (src.channels() == 3) {
            //handle 3-channel image
            if (rand() % 2 == 1) {
                dst->at<Vec3b>(r, c)[0] = 255;
                dst->at<Vec3b>(r, c)[1] = 255;
                dst->at<Vec3b>(r, c)[2] = 255;
            } else {
                dst->at<Vec3b>(r, c)[0] = 0;
                dst->at<Vec3b>(r, c)[1] = 0;
                dst->at<Vec3b>(r, c)[2] = 0;
            }
        } else if (src.channels() == 1) {
            //handle black&white image
            if (rand() % 2 == 1) {
                dst->at<uchar>(r, c) = 255;
            } else {
                dst->at<uchar>(r, c) = 0;
            }
        }
    }
    return true;
}

//medianblur use the median number of numbers in a square
//medianblur can filter salt & pepper noise because noise pixels are 255 or 0, surely not median number
bool MedianBlur(const Mat *src, Mat *dst, int size) {
    if (src->empty()) {
        fprintf(stderr, "src empty");
        return false;
    } else if (dst->empty()) {
        fprintf(stderr, "dst empty");
        return false;
    } else if (size % 2 == 0) {
        fprintf(stderr, "only odd size allowed");
        return false;
    }

    vector<Mat> channels;
    split(*src, channels);
    Mat channel;
    vector<uchar> square;

    for (int i = 0; i < channels.size(); i++) {
        channel = channels.at(i);
        for (int row = 0; row < channel.rows; row++) {
            for (int col = 0; col < channel.cols; col++) {
                //push the center of the square first
                square.push_back(src->at<Vec3b>(row, col)[i]);

                //sort the valid pixels in a square
                for (int row_in = row - size/2; row_in <= row + size/2; row_in++) {
                    for (int col_in = col - size/2; col_in <= col + size/2; col_in++) {
                        if (row_in < 0 || col_in < 0 || (row_in == row && col_in == col) ||
                                row_in > channel.rows - 1 || col_in > channel.cols - 1)
                            continue;

                        uchar num = src->at<Vec3b>(row_in, col_in)[i];
                        for (vector<uchar>::iterator iter = square.begin(); iter != square.end(); iter++) {
                            if (num < *iter) {
                                square.insert(iter, num);
                                break;
                            }
                            if (iter == square.end() - 1) {
                                square.push_back(num);
                                break;
                            }
                        }
                    }
                }

                //replace the output pixel with the midian number
                if (square.size() > 1) {
                    channel.at<uchar>(row, col) = square.at(square.size() / 2 + 1);
                }
                square.clear();
            }
        }
    }

    merge(channels, *dst);

    return true;
}

int main(int argc, char **argv) {
    /// Load the source image
    if (argc < 2) {
        fprintf(stderr, "no image specified");
        exit(-1);
    }
    Mat src = imread(argv[1], 1);
    if (src.empty()) {
        fprintf(stderr, "failed reading image");
        exit(-1);
    }
    Mat dst = src.clone();

    //original picture
    namedWindow("Unprocessed", WINDOW_AUTOSIZE);
    imshow("Unprocessed Image", src);

    //add noise
    AddSaltPepperNoise(src, &dst, 0.1);
    namedWindow("Processed", WINDOW_AUTOSIZE);
    imshow("Processed", dst);

    //use filter
    MedianBlur(&dst, &dst, 5);
    namedWindow("Blurred", WINDOW_AUTOSIZE);
    imshow("Blurred", dst);

    waitKey();
    return 0;
}